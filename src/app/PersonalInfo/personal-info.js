import React, { Component } from 'react'
import Navbar from "../../components/navbar";
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Button from '@material-ui/core/Button';
import { blue } from '@material-ui/core/colors';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
      },
    container: {
      display: 'grid',
      gridTemplateColumns: 'repeat(12, 1fr)',
      gridGap: theme.spacing(3),
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      whiteSpace: 'nowrap',
      marginBottom: theme.spacing(1),
    },
    divider: {
      margin: theme.spacing(2, 0),
    },
    bigAvatar: {
        margin: 10,
        width: 100,
        height: 100,
    },
    rightIcon: {
        marginRight: theme.spacing(1),
    },
    buttonUpload: {
    margin: theme.spacing(1),
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
      },
    menu: {
    width: 200,
    textAlign:left,
    formControl: {
        margin: theme.spacing(2),
        minWidth: 120,
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
    },
  }));
  
  const ColorButton = withStyles(theme => ({
    root: {
      color: theme.palette.getContrastText(blue[500]),
      backgroundColor: blue[500],
      '&:hover': {
        backgroundColor: blue[700],
      },
    },
  }))(Button);

  

  const currencies = [
    {
        value: '',
        label: '',
      },
    {
      value: 'USD',
      label: '$',
    },
    {
      value: 'EUR',
      label: '€',
    },
    {
      value: 'BTC',
      label: '฿',
    },
    {
      value: 'JPY',
      label: '¥',
    },
  ];
export default class PersonalInfo extends Component {
   handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
      };
    
      handleDelete() {
        alert('You clicked the delete icon.');
      }
    
    
    render() {
        return (
            <div className="page-container">
                <Navbar/>
                <div className="section">
                <Grid container spacing={3}>
                    <Grid  xs={3}> </Grid>
                    <Grid xs={9}>
                        <div className="title">Profile Picture</div>
                    </Grid>
                    
                    <Grid item xs={3}> </Grid>
                    <Grid item xs={1}>
                        <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" className={useStyles.bigAvatar} />
                    </Grid>
                    <Grid item xs={8}>
                    <ColorButton variant="contained" color="primary" className={useStyles.buttonUpload}>
                        <CloudUploadIcon className={useStyles.rightIcon} style={{marginRight:'5px'}}/>
                        Upload Photo
                        </ColorButton>
                    </Grid>
                  

                   <Grid  item xs={3}> </Grid>
                   <Grid item xs={2}>
                   <TextField
                        id="outlined-name"
                        label="Name"
                        className={useStyles.textField}
                        value="Name"
                        //onChange={handleChange('name')}
                        margin="normal"
                        variant="outlined"
                    />
                   </Grid>
                   <Grid item xs={2}>
                   <TextField
                        id="outlined-name"
                        label="Middlename"
                        className={useStyles.textField}
                        value="Middlename"
                        //onChange={handleChange('name')}
                        margin="normal"
                        variant="outlined"
                    />
                   </Grid>
                   <Grid item xs={5}>
                   <TextField
                        id="outlined-name"
                        label="Surename"
                        className={useStyles.textField}
                        value="Surename"
                        //onChange={handleChange('name')}
                        margin="normal"
                        variant="outlined"
                    />
                   </Grid>
                   

                   <Grid  item xs={3}> </Grid>
                   <Grid item xs={4}>
                   <b>Birthday date *</b>
                   <form className={useStyles.root} autoComplete="off">
                   <FormControl className={useStyles.formControl} style={{marginRight:'5px',width:'120px'}}>
                        <InputLabel htmlFor="age-simple">Day</InputLabel>
                        <Select
                       // value={values.age}
                       // onChange={handleChange}
                        inputProps={{
                            name: 'age',
                            id: 'age-simple',
                        }}
                        >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl className={useStyles.formControl} style={{marginRight:'5px',width:'120px'}}>
                        <InputLabel htmlFor="age-simple">Month</InputLabel>
                        <Select
                       // value={values.age}
                       // onChange={handleChange}
                        inputProps={{
                            name: 'age',
                            id: 'age-simple',
                        }}
                        >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl className={useStyles.formControl} style={{marginRight:'5px',width:'120px'}}>
                        <InputLabel htmlFor="age-simple">Year</InputLabel>
                        <Select
                       // value={values.age}
                       // onChange={handleChange}
                        inputProps={{
                            name: 'age',
                            id: 'age-simple',
                        }}
                        >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                    </form>
                   </Grid>
                   <Grid item xs={5}>
                   <b>Gender *</b>
                   <div>
                   <FormControlLabel value="male" control={<Radio />} label="Male" />
                   <FormControlLabel value="female" control={<Radio />} label="Female" />
                   </div>
                   </Grid>
                   <Grid  item xs={3}> </Grid>
                   <Grid item xs={9}>
                   <TextField
                        id="outlined-name"
                        label="Phone Number"
                        className={useStyles.textField}
                        value="+36171101"
                        //onChange={handleChange('name')}
                        margin="normal"
                        variant="outlined"
                    />
                   </Grid>

                   <Grid  item xs={3}> </Grid>
                   <Grid  item xs={9}><div className="languages" style={{fontSize:'34px'}}>Languages</div> </Grid>

                   <Grid  item xs={3}> </Grid>
                   <Grid  item xs={9}><div  style={{fontSize:'20px'}}>What Languages you speak ?</div> 
                   </Grid>
                   <Grid  item xs={3}> </Grid>
                   <Grid  item xs={9}>
                   <FormControl className={useStyles.formControl} style={{marginRight:'5px',width:'120px'}}>
                        <InputLabel htmlFor="age-simple">Languages</InputLabel>
                        <Select
                       // value={values.age}
                       // onChange={handleChange}
                        inputProps={{
                            name: 'age',
                            id: 'age-simple',
                        }}
                        >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                    <ColorButton variant="contained" color="primary" className={useStyles.buttonUpload} style={{marginTop:'12px'}}>
                        Save
                        </ColorButton>
                   </Grid>
                   <Grid  item xs={3}> </Grid>
                   <Grid  item xs={9}>
                    <Chip
                    label="english"
                    onDelete={this.handleDelete}
                    //className={classes.chip}
                    color="primary"
                        />
                    </Grid>
                    <Grid  item xs={3}> </Grid>
                    <Grid  item xs={5}> </Grid>
                   <Grid  item xs={4}>
                   <Button variant="contained" color="primary" className={useStyles.buttonUpload}>
                        Next Step
                        </Button>
                   </Grid>
                </Grid>
                </div>
            </div>
        );
    }
}
