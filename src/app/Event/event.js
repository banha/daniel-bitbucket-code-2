import React, {Component} from 'react';
import Navbar from "../../components/navbar";
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import calendar from '../../assets/calendar.svg'
import rowing from '../../assets/rowing.png'
import mentoring from '../../assets/mentoring.png'
import competition from '../../assets/competition.png'
import Button from '@material-ui/core/Button';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    buttonUpload: {
        margin: theme.spacing(1),
        },
   
  }));


  export default class Event extends Component {
    handleChange = name => event => {
         setValues({ ...values, [name]: event.target.value });
       };
     
       handleDelete() {
         alert('You clicked the delete icon.');
       }
     
     
     render() {
         return (
             <div className="page-container">
                 <Navbar/>
                 <div className="section">
                <Grid container spacing={3}>
                    <Grid item xs={1}> </Grid>
                    <Grid item xs={2}>
                    <div className="boxStep">
                    <div className="stepOne">Step 1 </div>
                    <div className="stepOneText">Choose Event Type</div>
                    </div></Grid>
                    <Grid item xs={9}>
                        <div className="title">Let's create new activity</div>
                    </Grid>

                    <Grid item xs={2}> </Grid>
                    <Grid item xs={10}>
                        <div className="heading">Which type of activity do you offer?</div>
                    </Grid>

                    <Grid item xs={2}> </Grid>
                    
                    <Grid item xs={1}>
                        <img src={calendar} style={{marginTop:20}}/>
                    </Grid>
                    <Grid item xs={3} >
                        <div className="heading">Event
                        <div className="content">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi</div>
                        </div>
                    </Grid>
                    
                    <Grid item xs={1}></Grid>

                    
                    <Grid item xs={1}>
                        <img src={rowing} style={{marginTop:20}}/>
                    </Grid>
                    <Grid item xs={3}>
                        <div className="heading">Training
                        <div className="content">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi</div>
                        </div>
                    </Grid>
                    
                    <Grid item xs={1}></Grid>

                    <Grid item xs={2}> </Grid>
                    
                    <Grid item xs={1}>
                        <img src={mentoring} style={{marginTop:20}}/>
                    </Grid>
                    <Grid item xs={3} >
                        <div className="heading">Mentoring
                        <div className="content">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi</div>
                        </div>
                    </Grid>
                    
                    <Grid item xs={1}></Grid>
                    
                    
                    <Grid item xs={1}>
                        <img src={competition} style={{marginTop:20}}/>
                    </Grid>
                    <Grid item xs={3}>
                        <div className="heading">Competition
                        <div className="content">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi</div>
                        </div>
                    </Grid>
                    
                    <Grid item xs={1}></Grid>
                    <Grid  item xs={3}> </Grid>
                    <Grid  item xs={6}> </Grid>
                   <Grid  item xs={3}>
                   <Button variant="contained" color="primary" className={useStyles.buttonUpload}>
                        Next Step
                        </Button>
                   </Grid>
                </Grid>
                </div>
             </div>
         );
     }
 }


