import React, {Component} from 'react';
import { palette } from '@material-ui/system';
import notificationicon from '../../assets/outline-notifications-24px.svg';
import imageright from '../../assets/bike-rider-blurred-background-daylight-1992384.png';
import loveicon from '../../assets/outline-love-24px.svg'
import {Link} from "react-router-dom";
import logo from '../../assets/logo.png';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Badge from '@material-ui/core/Badge';
import * as Icon from '@material-ui/icons';
const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  
}));

export default class DesktopNavbar extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
    }

    render() {
        return (

        <div className="desktop-navbar">
        <Grid container spacing={0} >
        <Grid item xs={10} sm={10}  >
            <Link className="desktop-logo" to="/">
                <img src={logo} alt="site logo"/>
                <h1 style={{color:'black'}}>Title</h1>          
            </Link>
        </Grid>
        <Grid item xs={2} sm={2}  >                
          <div className="navigation-menu">
            <ul>
                <li style={{marginRight:60}}>
                <Badge badgeContent={3} color="secondary">
                    <Icon.NotificationsOutlined />
                </Badge> 
                </li>
                <li style={{marginRight:60}}>
                <Badge badgeContent={6} color="secondary">
                    <Icon.Favorite />
                </Badge>
                </li>
                <li>
                    <img src={imageright} alt="site imageright"/>         
                </li>
            </ul>
          </div>
        </Grid>
      </Grid>
    </div>
        );
    }
}
